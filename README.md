# OpenMW-Deps

Archives needed to build OpenMW on Windows.

## Usage

If you need the files in this repo, clone it like so to avoid getting history that you don't need:

    git clone -b windows --depth 1 https://gitlab.com/hristoast/openmw-deps

## Adding Files

If you are adding a non-text file, be sure to track it with LFS before adding/committing:

    git lfs track YourNewFile.zip

Then add it as you would normally:

    git add YourNewFile.zip
